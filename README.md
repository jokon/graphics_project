# 3D Paintbrush and Model Deformation Tool, using OpenGL and GLUT. 

This project was implemented for the Computer Graphics and Virtual Reality course, taught by Prof. Konstantinos Moustakas in the University of Patras, Greece. 

The headCry.obj was found in Robert W. Sumner, Jovan Popovic, Deformation Transfer for Triangle Meshes Paper.

#### Installation

This project has been run using Visual Studio 2010 and GLUT.

#### Features of this project


**1)** Select a triangle of a mesh by clicking on it.


<img src="./images/image-000.png" width="30%">


**2)** Create a 3D brush by specifying its radius, brightness and opacity. With opacity = 0, the 3D brush colours both sides of the mesh.

- _brightness =1 and opacity = 0_

<img src="./images/image-004.png" width="30%">     <img src="./images/image-005.png" width="32%">


- _brightness =1 and opacity = 1_

<img src="./images/image-006.png" width="30%">     <img src="./images/image-007.png" width="30%">

**3)** Save the coloured mesh in a txt file to be able to load it at a later time.

**4)** Create a modified 3D brush that colours the vertices of each triangle taking into account their _weights_. The _weight_ of each vertex is calculated based on its distance from the 3D brush axis.

- _brightness = 0.6 and opacity = 1_

<img src="./images/image-018.png" width="30%">     <img src="./images/image-019.png" width="30%">

**5)** Select an anchor point of the mesh, colour the mesh and deform the coloured region taking into account the vertices' weights.The direction of the mouse-drag defines the direction of the deformation. 

- _no colouring of the mesh --> no deformation_

<img src="./images/image-020.png" width="30%">

- _opacity = 1 and strength = 1_

<img src="./images/image-022.png" width="30%">

- _opacity = 1 and strength = 1_

<img src="./images/image-024.png" width="30%">

- _opacity = 1 and strength = 1_

<img src="./images/image-026.png" width="30%">

**6)** Save the deformed mesh in a txt file.

#### License 

Copyright © 2016 Ioanna Kontou

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
