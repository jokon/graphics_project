/*  
    graphics_project
    Copyright (C) 2016  Ioanna Kontou

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

*/


//----------------------------------------------------------------------------//
// Apallaktikh ergasia sta grafika No3                                        //
//----------------------------------------------------------------------------//

#include <stdlib.h>	  // Just for some standard functions
#include <stdio.h>    // Just for some ASCII messages
#include <glut.h>     // An interface and windows management library
#include "visuals.h"  // Header file for our OpenGL functions

/*******************************************************************************
  Main Program code
 ******************************************************************************/

int main(int argc, char* argv[])
{
    // initialize GLUT library state
    glutInit(&argc, argv);

    // Set up the display using the GLUT functions to
    // get rid of the window setup details:
    // - Use true RGB colour mode ( and transparency )
    // - Enable double buffering for faster window update
    // - Allocate a Depth-Buffer in the system memory or
    //   in the video memory if 3D acceleration available
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE);

    // Define the main window size and initial position
    // ( upper left corner, boundaries included )
    glutInitWindowSize(480, 480);
    glutInitWindowPosition(50, 50);

    // Create and label the main window
    glutCreateWindow("Graphics Apallaktikh No3");

    // Configure various properties of the OpenGL rendering context
    setup();

    // Callbacks for the GL and GLUT events:
    // Set the rendering function
    glutDisplayFunc(render);
    // Set the resize function
    glutReshapeFunc(resize);
    // Set the idle function
    glutIdleFunc(idle);

	//Set function for the menu
	glutCreateMenu(menuSelect);
	glutAddMenuEntry("Ex1A:Ray-Cast Picking", EX1A);
    glutAddMenuEntry("Ex2A:3D-brush", EX2A);
	glutAddMenuEntry("Ex1B:Modified-3D-brush", EX1B);
	glutAddMenuEntry("Ex2B:Select anchor point", EX2B);
	glutAddMenuEntry("Ex3B:Simple Deformation tool", EX3B);
	glutAddMenuEntry("Ex4B:Handle Deformation Of Non-Overlapped Brushed Regions", EX4B);
    // attach the menu to the right button
    glutAttachMenu(GLUT_RIGHT_BUTTON);

    // Set functions for keyboard
    glutKeyboardFunc(keyboardDown);
    glutKeyboardUpFunc(keyboardUp);
    glutSpecialFunc(keyboardSpecialDown);
    glutSpecialUpFunc(keyboardSpecialUp);

    // Set functions for mouse
    glutMouseFunc(mouseClick);
    glutMotionFunc(mouseMotion);

    //Enter main event handling loop
    glutMainLoop();
    return 0;
}

