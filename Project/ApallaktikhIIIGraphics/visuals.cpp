/*
    graphics_project
    Copyright (C) 2016  Ioanna Kontou

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

//----------------------------------------------------------------------------//
//  Apallaktikh ergasia sta grafika No3                                       //                                                        
//----------------------------------------------------------------------------//

#include <iostream>	  // For string handling
#include <stdlib.h>   // Just for some standard functions
#include <stdio.h>    // Just for some ASCII messages
#include <glut.h>     // An interface and windows management library
#include "visuals.h"  // Header file for our OpenGL functions
#include "windows.h"
#include "tiny_obj_loader.h"
#include "soil.h"

#define KEY_ESC 27
#define PI 3.1415927
#define BRUSH_R 0.0
#define BRUSH_G 1.0
#define BRUSH_B 0.0

using namespace std;
using namespace tinyobj;

/*******************************************************************************
  State Variables
 ******************************************************************************/

// Menu mode variable
static int mode = 0;



// Model variables
mesh_t mesh;
vector<float> vertex_color_of_mesh_vertices_ex1(0);//ray-cast picking
vector<float> vertex_color_of_mesh_vertices_ex2(0);//brush-3d
static float dx = 0.0;
static float dy = 0.0;
static float dz = 0.0;
static float ry = 0.0;
// Camera variables
static float rotFactor = 0.001;
static float zoomStep  = 10;
static float cdist = 250.0;
static float cx = 0.0;
static float cy = 0.0;
static float cz = cdist;
static float azimuthAngle  = 0.0;
static float altitudeAngle = PI/2.;
// Event handle variables
static bool mouseClickDown = false;
static int  mx0;
static int  my0;
vector<int> mouseClickCoordsVector(0);
//menu bool variables
bool checkForCollision = false;
bool brushOn  = false;
bool newBrush = false;
//brush-3d parameters
//their scope is global, because if they are declared in render, each time render is called, the values of these variables are lost
vector<float> weightOfEveryVertexOfMesh(0);
float ray, strength;
int opacity;
//anchor point
float anchorPoint[3];
bool anchorPointExists = false;
bool mouseClickForRayCastCheck = false;
//deformation
bool cameraOn = false;
bool deformationOn = true;
mesh_t meshInitial;
int timeSinceStart = 0;
int previousSinceStartTime = 0;
int timeNeededForSimpleDeformation = 0;
//handle non-overlapped brushed regions' deformation
vector<int> indexToIndicesVectorToTrisBrushedByBrush3D(0);
bool handleDeformationOfNonOverlappedBrushedRegions = false;
//flags
bool saveModifiedModelToFile   = false;
bool loadModifiedModelFromFile = false;
bool clearColorMadeByBrush = false;
bool selectAnchorPointOn   = false;

/*******************************************************************************
  Implementation of Auxiliary Functions
 ******************************************************************************/



string getExeDir()
{
    HMODULE hModule = GetModuleHandleW(NULL);
    WCHAR wPath[MAX_PATH];
    GetModuleFileNameW(hModule, wPath, MAX_PATH);
    char cPath[MAX_PATH];
    char DefChar = ' ';
    WideCharToMultiByte(CP_ACP, 0, wPath, -1, cPath, 260, &DefChar, NULL);
    string sPath(cPath);
    return sPath.substr(0, sPath.find_last_of("\\/")).append("\\");
}

void readObj(string filename, mesh_t &mesh)
{
    vector<shape_t> shapes;
    string err = LoadObj(shapes, filename.c_str(), (getExeDir() + "..\\Models\\").c_str());

    if (!err.empty())
    {
        cerr << err << endl;
        exit(1);
    }
    else
    {       
        mesh = shapes[0].mesh;
        if (mesh.normals.empty())
        {
            cout << "Normals do not exist!" << endl;
        }
    }
}



void drawObj(mesh_t &mesh, vector<float> color_of_mesh_vertex)
{
    vector<unsigned> &indices = mesh.indices;
    vector<float>  &positions = mesh.positions;
    vector<float>    &normals = mesh.normals;

    float* point;
    float* normal;
	float* color;

    glBegin(GL_TRIANGLES);
    for(int i = 0; i < indices.size(); i++)
    {
		if(!normals.empty())
			normal = &normals[3*indices[i]];
        point = &positions[3*indices[i]];
		color = &color_of_mesh_vertex[3*indices[i]];

		if(!normals.empty())
			glNormal3fv((GLfloat*) normal);

		glColor3f((GLfloat) color[0], (GLfloat) color[1], (GLfloat) color[2]);
        glVertex3fv((GLfloat*) point);
    }
    glEnd();
}



float Dot_product(GLdouble v1_x, GLdouble v1_y, GLdouble v1_z, GLdouble v2_x, GLdouble v2_y,GLdouble v2_z)
{
	return v1_x * v2_x + v1_y * v2_y + v1_z * v2_z; 
}

void Cross_product(GLfloat *cross_p ,GLdouble v1_x, GLdouble v1_y, GLdouble v1_z, GLdouble v2_x, GLdouble v2_y,GLdouble v2_z)
{
	cross_p[0] = v1_y * v2_z - v1_z * v2_y ;
	cross_p[1] = v1_z * v2_x - v1_x * v2_z ;
	cross_p[2] = v1_x * v2_y - v1_y * v2_x ;
}

void Min(GLfloat &min, int &index_of_min, GLfloat *table, int table_size)
{
	index_of_min = 0;
	min = table[0];
	for(int i = 1 ; i < table_size ; i ++)
	{
		if(table[i] < min)
		{
			index_of_min = i;
			min = table[i];
		}
	}

}

void Max(GLfloat &max, int &index_of_max, GLfloat *table, int table_size)
{
	index_of_max = 0;
	max = table[0];
	for(int i = 1 ; i < table_size ; i++ )
	{
		if(table[i] > max)
		{
			index_of_max = i;
			max = table[i];
		}
	}

}

void Minv(GLfloat &min, int &index_of_min, vector<GLdouble> vec)
{
	index_of_min = 0;
	min = vec[0];
	for(int i = 1 ; i < vec.size() ; i ++)
	{
		if(vec[i] < min)
		{
			index_of_min = i;
			min = vec[i];
		}
	}

}

void Max(GLfloat &max, int &index_of_max, vector<GLfloat> vec)
{
	index_of_max = 0;
	max = vec[0];
	for(int i = 1 ; i < vec.size() ; i++ )
	{
		if(vec[i] > max)
		{
			index_of_max = i;
			max = vec[i];
		}
	}

}


/////////////den to xw xrhsimopoihsei pouthena mexri stigmhs, an sunexistei auto na th svhsw th sunarthsh
void FindNormalizedVector3DGiven2Points3D(double *norm_vec, double *vecBegin, double *vecEnd)
{
	double dir[3] = {vecEnd[0] - vecBegin[0], vecEnd[1] - vecBegin[1], vecEnd[2] - vecBegin[2]};
	double length = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2],2));
	
	norm_vec[0] = dir[0] / length;
	norm_vec[1] = dir[1] / length;
	norm_vec[2] = dir[2] / length;
}

/////////////////functions needed for ray-cast picking/////////////////////////////////
void get3DRay(GLdouble *vecBegin, GLdouble *vecEnd, int x_screen_coord, int y_screen_coord)
{
	//find begin and end points of 3d ray
	GLdouble modelParam[16] ;
	GLdouble projectionParam[16] ;
	GLint viewParam[4] ;
	
	glGetDoublev(GL_MODELVIEW_MATRIX,modelParam);
	glGetDoublev(GL_PROJECTION_MATRIX,projectionParam);
	glGetIntegerv(GL_VIEWPORT,viewParam);
	
	GLdouble y = viewParam[3] - y_screen_coord;
	
	if(!gluUnProject(x_screen_coord,y,0,modelParam,projectionParam,viewParam,vecBegin,vecBegin+1,vecBegin+2))
		cout << "failure during trying to get vec3dBegin .." << endl;
	if(!gluUnProject(x_screen_coord,y,1,modelParam,projectionParam,viewParam,vecEnd,vecEnd+1,vecEnd+2))
		cout << "failure during trying to get vec3dEnd .." << endl;
}

void MassCenterOfMesh(mesh_t &mesh, double *mass_center){
	vector<unsigned> &indices = mesh.indices;
	vector<float> &positions = mesh.positions;
   
	float ptx = 0;
	float pty = 0;
	float ptz = 0;
	for(int i = 0 ; i < mesh.positions.size(); i+=3){
		ptx+=positions[i];
		pty+=positions[i+1];
		ptz+=positions[i+2];
	}
	mass_center[0] = 3*ptx/positions.size();
	mass_center[1] = 3*pty/positions.size();
	mass_center[2] = 3*ptz/positions.size();
}

float RadiusOfSphere(mesh_t &mesh, double *mass_center)
{
	vector<unsigned> &indices = mesh.indices;
	vector<float> &positions = mesh.positions;
	
	//find point with absolute max x,y,z coords
	
	//Step1 : find maxX, maxY, maxZ, minX, minY, minZ
	float maxX = positions[0];
	float maxY = positions[1];
	float maxZ = positions[2];
	
	float minX = positions[0];
	float minY = positions[1];
	float minZ = positions[2];
	
	for(int i = 3 ; i < positions.size() ; i += 3)
	{
		if(positions[i] > maxX)
			maxX = positions[i];
		if(positions[i + 1] > maxY)
			maxY = positions[i + 1] ;
		if(positions[i + 2] > maxZ)
			maxZ = positions[i + 2];
		if(positions[i] < minX)
			minX = positions[i];
		if(positions[i + 1] < minY)
			minY = positions[i + 1];
		if(positions[i + 2] < minZ)
			minZ = positions[i + 2];

	}
	//Step2 : take absolute values of minX, minY, minZ, maxX, maxY, maxZ 
	minX = abs(minX);
	minY = abs(minY);
	minZ = abs(minZ);

	maxX = abs(maxX);
	maxY = abs(maxY);
	maxZ = abs(maxZ);

	//Step3 : check for the greater one
	if(minX > maxX)
		maxX = minX;
	if(minY > maxY)
		maxY = minY;
	if(minZ > maxZ)
		maxZ =minZ;
	
	//Step4 : compute radius -> distance of mass_center and the point with the greatest absolute values
	return sqrt(pow(mass_center[0] - maxX, 2) + pow(mass_center[1] - maxY, 2) + pow(mass_center[2] - maxZ, 2));
}


void DistanceOfA3DLineAndA3DPoint(double &distance, double *vecBegin, double *vecEnd, double *point)
{
	//Step1 : find vectors (vecEnd - vecBegin), (point- vecBegin) 
	double vec21[3] = {vecEnd[0] - vecBegin[0], vecEnd[1] - vecBegin[1], vecEnd[2] - vecBegin[2]};
	double vec23[3] = {point[0] - vecBegin[0], point[1]- vecBegin[1], point[2]- vecBegin[2]};

	//Step2 : find vec23 x vec21
	float cross_product[3];
	Cross_product(cross_product, vec23[0], vec23[1], vec23[2], vec21[0], vec21[1], vec21[2]);
	
	//Step3 : find length of cross product
	float crossProductLength = sqrt(pow(cross_product[0], 2) + pow(cross_product[1], 2) + pow(cross_product[2], 2));
	
	float vec21Length = sqrt(pow(vec21[0], 2) + pow(vec21[1], 2) + pow(vec21[2], 2));

	//Step4 : distance = |cross_product| / |endPt - beginPt| :
	distance = crossProductLength / vec21Length;
}

bool Ray3DIntersectsWithSphere(double *mass_center,float radius_of_sphere,GLdouble *vecBegin,GLdouble *vecEnd)
{
	double distance;
	DistanceOfA3DLineAndA3DPoint(distance, vecBegin, vecEnd, mass_center);
		
	if(radius_of_sphere >= distance)
		return true;
	else
		return false;
}


void PlaneEquation1(float *a,float *b,float *c,float *d,float *vert1,float *vert2,float *vert3)
{
	//find plane equation given three points 3d that define the plane
	//vertices of each triangle are counter-clock wise, because it's an assumption of obj files 
	//Step1 : define vecAB,vecAC
	float vecAB_x = vert2[0] - vert1[0];
	float vecAB_y = vert2[1] - vert1[1];
	float vecAB_z = vert2[2] - vert1[2];

	float vecAC_x = vert3[0] - vert1[0];
	float vecAC_y = vert3[1] - vert1[1];
	float vecAC_z = vert3[2] - vert1[2];

	//Step2 : cross_product of vecAB,vecAC
	//cross_product_x -> *a
	//cross_product_y -> *b
	//cross_product_z -> *c
	Cross_product(a, vecAB_x, vecAB_y, vecAB_z, vecAC_x, vecAC_y, vecAC_z);
	
	*d = -(*a)*vert1[0] - (*b)*vert1[1] - (*c)*vert1[2];
}

bool Ray3DIntersectsWithPlaneOfTriangle(double *vecBegin, double *vecEnd, float *vert1, float *vert2, float *vert3)
{
	//Step1 : find vectors
	float v1_x = vecBegin[0];
	float v1_y = vecBegin[1];
	float v1_z = vecBegin[2];

	float v2_x = vecEnd[0];
	float v2_y = vecEnd[1];
	float v2_z = vecEnd[2];

	float v12_x = v2_x - v1_x;
	float v12_y = v2_y - v1_y;
	float v12_z = v2_z - v1_z;

	float vecAB_x = vert2[0] - vert1[0];
	float vecAB_y = vert2[1] - vert1[1];
	float vecAB_z = vert2[2] - vert1[2];

	float vecAC_x = vert3[0] - vert1[0];
	float vecAC_y = vert3[1] - vert1[1];
	float vecAC_z = vert3[2] - vert1[2];

	//Step2 : cross_product of vecAB,vecAC
	float cross_p[3];
	Cross_product(cross_p, vecAB_x, vecAB_y, vecAB_z, vecAC_x, vecAC_y, vecAC_z);
	

	//Step3 : dot_product of normal of plane and ray3d
	float dot_prod = Dot_product(cross_p[0], cross_p[1], cross_p[2], v12_x, v12_y, v12_z);

	if(abs(dot_prod) <= 0.001)
		return false;
	else return true;
}

void IntersectionPointOfPlaneAndRay3D(GLfloat *int_pt,GLdouble *vecBegin,GLdouble *vecEnd,GLdouble *startingPtOfRay,GLfloat a,GLfloat b,GLfloat c, GLfloat d)
{
	GLfloat r = sqrt(pow(vecEnd[0] - vecBegin[0], 2) + pow(vecEnd[1] - vecBegin[1], 2) + pow(vecEnd[2] - vecBegin[2], 2));
	GLfloat dir_x = (vecEnd[0] - vecBegin[0]) / r;
	GLfloat dir_y = (vecEnd[1] - vecBegin[1]) / r;
	GLfloat dir_z = (vecEnd[2] - vecBegin[2]) / r;
	GLfloat t = (- d - a * startingPtOfRay[0] - b * startingPtOfRay[1] - c * startingPtOfRay[2]) / (a * dir_x + b * dir_y + c * dir_z);
	
	int_pt[0] = startingPtOfRay[0] + t * dir_x;
	int_pt[1] = startingPtOfRay[1] + t * dir_y;
	int_pt[2] = startingPtOfRay[2] + t * dir_z;

}

bool intersectionPtIsInsideTheTriangle(float *int_pt,float *v1,GLfloat *v2,float *v3)
{

	//Step1 : find vectors v12,v23,v31,v1p,v2p,v3p
	float v12_x = v2[0] - v1[0];
	float v12_y = v2[1] - v1[1];
	float v12_z = v2[2] - v1[2];

	float v23_x = v3[0] - v2[0];
	float v23_y = v3[1] - v2[1];
	float v23_z = v3[2] - v2[2];

	float v31_x = v1[0] - v3[0];
	float v31_y = v1[1] - v3[1];
	float v31_z = v1[2] - v3[2];

	float v1p_x = int_pt[0] - v1[0];
	float v1p_y = int_pt[1] - v1[1];
	float v1p_z = int_pt[2] - v1[2];

	float v2p_x = int_pt[0] - v2[0];
	float v2p_y = int_pt[1] - v2[1];
	float v2p_z = int_pt[2] - v2[2];

	float v3p_x = int_pt[0] - v3[0];
	float v3p_y = int_pt[1] - v3[1];
	float v3p_z = int_pt[2] - v3[2];
	
	//Step2 : compute the following cross products
	
	float cross_1[3];
	float cross_2[3];
	float cross_3[3];
	Cross_product(cross_1, v12_x, v12_y, v12_z, v1p_x, v1p_y, v1p_z);
	Cross_product(cross_2, v23_x, v23_y, v23_z, v2p_x, v2p_y, v2p_z);
	Cross_product(cross_3, v31_x, v31_y, v31_z, v3p_x, v3p_y, v3p_z);


	//Step3 : if all cross_products are on the same side => the intersection point is inside the triangle
	if((cross_1[0] * cross_2[0] > 0) && (cross_1[0] * cross_3[0] > 0) && (cross_2[0] * cross_3[0] > 0) && 
		(cross_1[1] * cross_2[1] > 0) && (cross_1[1] * cross_3[1] > 0) && (cross_2[1] * cross_3[1] > 0) &&
		(cross_1[2] * cross_2[2] > 0) && (cross_1[2] * cross_3[2] > 0) && (cross_2[2] * cross_3[2] > 0))
		return true;
	else return false;
}

bool TriIntersectsWithRay3D(float *vert1,float *vert2,float *vert3,double *vecBegin,double *vecEnd,double *startingPtOfRay,vector<float> &intersection_pts)
{
	float a[4];
	PlaneEquation1(a, a + 1, a + 2, a + 3, vert1, vert2 , vert3);
	if(Ray3DIntersectsWithPlaneOfTriangle(vecBegin, vecEnd, vert1, vert2, vert3))
	{
		float intersection_pt[3];
		IntersectionPointOfPlaneAndRay3D(intersection_pt, vecBegin, vecEnd, startingPtOfRay, a[0], a[1], a[2], a[3]);
		if(intersectionPtIsInsideTheTriangle(intersection_pt, vert1, vert2, vert3))
		{
			intersection_pts.push_back(intersection_pt[0]);
			intersection_pts.push_back(intersection_pt[1]);
			intersection_pts.push_back(intersection_pt[2]);
			return true;
		}
	}
	else return false;
} 

void TriangleClickedByMouse(int *index_of_tri_requested, float *intersection_pt, vector<int> &index_of_tri_intersected, vector<GLfloat> &intersection_pt_of_tris_and_ray3d,GLdouble *vecBegin)
{
	//check which of the intersection points selected is the closest to vecBegin pt
	//Step1 : compute and compare the squares of distances, to avoid sqrt that slows down performance 
	vector<double> square_of_distances(0);
	for(int j = 0 ; j < intersection_pt_of_tris_and_ray3d.size() ; j += 3)
	{
		float square = pow(vecBegin[0] - intersection_pt_of_tris_and_ray3d[j],2) + pow(vecBegin[1] - intersection_pt_of_tris_and_ray3d[j + 1],2) + pow(vecBegin[2] - intersection_pt_of_tris_and_ray3d[j + 2],2);
		square_of_distances.push_back(square);
	}

	//Step2 : find the min of square_of_distances
	if(!square_of_distances.empty())
	{
		int index;
		float min;
		Minv(min, index, square_of_distances);

		//Step3 : find triangle chosen/clicked by mouse
		*index_of_tri_requested = index_of_tri_intersected[index]; 
		//Step4 : find intersection_pt chosen(closest)
		intersection_pt[0] = intersection_pt_of_tris_and_ray3d[3*index];
		intersection_pt[1] = intersection_pt_of_tris_and_ray3d[3*index + 1];
		intersection_pt[2] = intersection_pt_of_tris_and_ray3d[3*index + 2];
	}

}



bool rayCastPicking(GLdouble *vecBegin, GLdouble *vecEnd, int &index_of_indices_vector_that_shows_the_intersected_tri,float *intersection_pt, int screen_coords_x, int screen_coords_y)
{
	//Step1 : find world coordinates(near and far clipping plane's intersection points with ray3d) of mouse's 'pixel' coords
	get3DRay(vecBegin, vecEnd, screen_coords_x, screen_coords_y);
	
	//Step2 : define bounding sphere of mesh 
	double mass_center[3];
	
	MassCenterOfMesh(mesh,mass_center);
	float radius_of_sphere =  RadiusOfSphere(mesh,mass_center);

	//Step3 : check for possibility of intersection between ray3d and sphere
	if(Ray3DIntersectsWithSphere(mass_center,radius_of_sphere,vecBegin,vecEnd))
	{
		//Step4 : above intersection true, so check for intersection of ray and each triangle of mesh
		vector<unsigned> &indices = mesh.indices;
		vector<float> &positions = mesh.positions;
		vector<int> index_of_tri_intersected(0);
		vector<float> intersection_pt_of_tris_and_ray3d(0);
		
		for(int i = 0 ; i < mesh.indices.size() ; i+=3)
		{
			float vert1[3] = {positions[3*indices[i]], positions[3*indices[i] + 1], positions[3*indices[i] + 2]};
			float vert2[3] = {positions[3*indices[i + 1]], positions[3*indices[i + 1] + 1], positions[3*indices[i + 1] + 2]};
			float vert3[3] = {positions[3*indices[i + 2]], positions[3*indices[i + 2] + 1], positions[3*indices[i + 2] + 2]};
			
			if(TriIntersectsWithRay3D(vert1,vert2,vert3,vecBegin,vecEnd,vecBegin,intersection_pt_of_tris_and_ray3d))
			{
				index_of_tri_intersected.push_back(i);	
			}
		}

		//Step5 : if there are intersection points, find the one closest to the camera -> closest to vecBegin
		if(!intersection_pt_of_tris_and_ray3d.empty())
		{
			int index = 0;
			TriangleClickedByMouse(&index,intersection_pt,index_of_tri_intersected,intersection_pt_of_tris_and_ray3d,vecBegin);
				
			index_of_indices_vector_that_shows_the_intersected_tri = index;

			return true;
		}
		cout << endl << "collision of ray and mesh : false!" << endl;
		return false;
	}
	else
	{
		cout << endl << "sphere and ray3d collision : false!" << endl;
		return false;
	}
}

void colorTheTriangleClickedByMouse(float *vert1_of_picked_tri, float *vert2_of_picked_tri, float *vert3_of_picked_tri)
{
	glPushMatrix();
	glBegin(GL_TRIANGLES);
	glColor3f(1,0,0);
	glVertex3f(vert1_of_picked_tri[0], vert1_of_picked_tri[1], vert1_of_picked_tri[2]);
	glVertex3f(vert2_of_picked_tri[0], vert2_of_picked_tri[1], vert2_of_picked_tri[2]);
	glVertex3f(vert3_of_picked_tri[0], vert3_of_picked_tri[1], vert3_of_picked_tri[2]);
	glEnd();
	glPopMatrix();
					
}

void highLightPickedTri(mesh_t meshObj)
{
	GLdouble vecBegin[3],vecEnd[3];
	GLfloat intersection_pt[3];
	int index_of_indices_vector_that_shows_the_intersected_tri = 0;

	if(rayCastPicking(vecBegin, vecEnd, index_of_indices_vector_that_shows_the_intersected_tri, intersection_pt, mx0, my0))
	{	
		float vert1_of_picked_tri[3];
		float vert2_of_picked_tri[3];
		float vert3_of_picked_tri[3];
		
		int index = index_of_indices_vector_that_shows_the_intersected_tri;
		vector<unsigned int> indices = meshObj.indices;
		vector<float> positions = meshObj.positions;

		vert1_of_picked_tri[0] = positions[3*indices[index]];
		vert1_of_picked_tri[1] = positions[3*indices[index] + 1]; 
		vert1_of_picked_tri[2] = positions[3*indices[index] + 2];

		vert2_of_picked_tri[0] = positions[3*indices[index + 1]]; 
		vert2_of_picked_tri[1] = positions[3*indices[index + 1] + 1];
		vert2_of_picked_tri[2] = positions[3*indices[index + 1] + 2];

		vert3_of_picked_tri[0] = positions[3*indices[index + 2]];
		vert3_of_picked_tri[1] = positions[3*indices[index + 2] + 1];
		vert3_of_picked_tri[2] = positions[3*indices[index + 2] + 2];
		
		colorTheTriangleClickedByMouse(vert1_of_picked_tri, vert2_of_picked_tri, vert3_of_picked_tri);
	}
}

/////////////////end of functions needed for ray-cast picking//////////////////////



/////////////////functions needed for 3D-brush/////////////////////////////////////


void initializeTheColorVectorOfMeshVertices(float *value, vector<float> &vec_of_colors)
{
	for(int  i = 0 ; i < mesh.positions.size() ; i += 3)
	{
		vec_of_colors.push_back(value[0]);
		vec_of_colors.push_back(value[1]);
		vec_of_colors.push_back(value[2]);
	}

}


void askTheUserToEnterTheAttributesOf3dBrusher(float &ray, float &strength, int &opacity)
{
	
	cout << endl << "Enter ray of 3d-brush: " << endl;
	cin  >> ray;
	
	do
	{
		cout << endl << "Enter strength of 3d-brush (range from 0 to 1): " << endl;
		cout << "If not, type again!" << endl;
		cin  >> strength;
	}while(strength < 0 || strength >1);

	
	do
	{
		cout << endl << "Enter opacity of 3d-brush" << endl; 
		cout << "(1 or 0 only approved as an aswer, for coloring only visible vertices or all vertices inside the brush_cylinder  equivalent): " << endl;
		cout << "If not 0 or 1, type again!" << endl;
		cin  >> opacity;
	}while(opacity != 0 && opacity != 1);

}



void defineCylinderThatRepresentsThe3dBrush(float *directionOfCylinder, double *vecBegin, double *vecEnd)
{
	//define direction of cylinder's axis
	double sub[3] = {vecEnd[0] - vecBegin[0], vecEnd[1] - vecBegin[1], vecEnd[2] - vecBegin[2]};
	double length = sqrt(pow(sub[0], 2) + pow(sub[1], 2) + pow(sub[2], 2));

	directionOfCylinder[0] = sub[0] / length;
	directionOfCylinder[1] = sub[1] / length;
	directionOfCylinder[2] = sub[2] / length;
}


bool Point3DIsInsideTheCylinder(double ray, double distance_of_pt_and_axis_of_cylinder)
{
	if(distance_of_pt_and_axis_of_cylinder <= ray)
		return true;
	return false;
}





void findVerticesOfMeshThatAreInsideTheCylinderAndSaveTheirDistanceFromCylinderAxis(double *vecBegin, double *vecEnd, float ray, vector<int> &indexVectorToPositionsVectorVerticesInsideTheCylinder, vector<float> &distanceVectorOfVerticesInsideTheCylinder)
{
	vector<float> &positions = mesh.positions;
	int j = 0;
	for(int i = 0 ; i < positions.size() ; i += 3)
	{
		double distance;
		double pt[3] = {positions[i], positions[i + 1], positions[i + 2]}; 
		DistanceOfA3DLineAndA3DPoint(distance, vecBegin, vecEnd, pt);
		
		if(Point3DIsInsideTheCylinder(ray,distance))
		{
			j++;
			
			indexVectorToPositionsVectorVerticesInsideTheCylinder.push_back(i);
			distanceVectorOfVerticesInsideTheCylinder.push_back(distance);
		
		}
	
	}

	cout << endl << "number of vertices that are inside the cylinder : " << j;

}

void findParameterFromLine3DEquation(float &parameter, float *intersection_pt, double *pt_of_mesh, float *direction)
{
	parameter = (intersection_pt[0] - pt_of_mesh[0]) / direction[0]; 

}


bool VertexIsVisibleByCamera( double *vecBegin, double *vecEnd, double *pt_of_mesh, float *direction_of_cylinder_axis)
{
	//ray from pt_of_mesh and direction the one of the camera
	vector<float> &positions = mesh.positions;
	vector<unsigned int> &indices = mesh.indices;
	vector<float> intersection_pt_of_tris_and_ray3d(0);

	for(int i = 0 ; i < indices.size() ; i += 3)
	{
		float vert1[3] = {positions[3*indices[i]], positions[3*indices[i] + 1], positions[3*indices[i] + 2]};
		float vert2[3] = {positions[3*indices[i + 1]], positions[3*indices[i + 1] + 1], positions[3*indices[i + 1] + 2]};
		float vert3[3] = {positions[3*indices[i + 2]], positions[3*indices[i + 2] + 1], positions[3*indices[i + 2] + 2]};
			
		if(TriIntersectsWithRay3D(vert1, vert2, vert3, vecBegin, vecEnd, pt_of_mesh, intersection_pt_of_tris_and_ray3d))
		{
			int size = intersection_pt_of_tris_and_ray3d.size();
			float inters_pt[3] = {intersection_pt_of_tris_and_ray3d[size - 3], intersection_pt_of_tris_and_ray3d[size -2], intersection_pt_of_tris_and_ray3d[size - 1]};
			float parameter;
			findParameterFromLine3DEquation(parameter, inters_pt, pt_of_mesh, direction_of_cylinder_axis);

			if(parameter < 0.01)
				return false; 
		}
	}
	return true;

}



void findIfVerticesInsideTheCylinderAreVisibleByCamera(vector<bool> &vertexInsideTheCylinderIsVisibleByCamera, double *vecBegin, double *vecEnd, float *directionOfCylinderAxis, vector<int> indexVectorToPositionsVectorVerticesInsideTheCylinder)
{
	vector<float> normals = mesh.normals;
	vector<int> index =  indexVectorToPositionsVectorVerticesInsideTheCylinder;

	
	for(int i = 0 ; i < index.size() ; i++)
	{
		float normal[3] = {normals[index[i]], normals[index[i] + 1], normals[index[i] + 2]};
		float dot_product = Dot_product(directionOfCylinderAxis[0], directionOfCylinderAxis[1], directionOfCylinderAxis[2], normal[0], normal[1], normal[2]);
		if(dot_product < 0)
		{
			//check if in vertexInsideTheCylinderIsVisibleByTheCamera some vertices are wrongly computed visible, cause they're overlapped by other triangles although their normal is "antirropo" with camera's normal 
			vector<float> positions = mesh.positions;
			double mesh_pt[3] = {positions[index[i]], positions[index[i] + 1], positions[index[i] + 2]};

			if(VertexIsVisibleByCamera(vecBegin, vecEnd, mesh_pt, directionOfCylinderAxis))
				vertexInsideTheCylinderIsVisibleByCamera.push_back(true);
			else
				vertexInsideTheCylinderIsVisibleByCamera.push_back(false);
		}
		else
			vertexInsideTheCylinderIsVisibleByCamera.push_back(false);
	}

}

void RGBToHSL(float *HSL, float *RGB)
{
	//find max and min of RGB 
	float min,max;
	int index_of_min,index_of_max;
	Min(min, index_of_min, RGB, 3);
	Max(max, index_of_max, RGB, 3);

	float D = max - min;

	//hue
	if( D < 0.001)
		HSL[0] = 0;
	else if(abs(max - RGB[0]) < 0.001)
		HSL[0] = 60 * fmod(((RGB[1] - RGB[2]) / D),6);
	else if(abs(max - RGB[1]) < 0.001)
		HSL[0] = 60 * ((RGB[2] - RGB[0]) / D + 2);
	else if(abs(max - RGB[2]) < 0.001)
		HSL[0] = 60 * ((RGB[0] - RGB[1]) / D + 4);

	//lightness
	HSL[2] = (max + min) / 2; 

	//saturation
	if(abs(D) < 0.001)
		HSL[1] = 0;
	else 
		HSL[1] = D/(1 - abs(2 * HSL[2] - 1));

}

void HSLToRGB(float *RGB, float *HSL)
{
	float C = (1 - abs(2 * HSL[2] - 1)) * HSL[1];
	float X = C * (1 - abs(fmod(HSL[0] / 60, 2) - 1));
	float m = HSL[2] - C / 2;

	if(HSL[0] >= 0 && HSL[0] < 60)
	{
		RGB[0] = C;
		RGB[1] = X;
		RGB[2] = 0;
	}
	else if(HSL[0] >= 60 && HSL[0] < 120)
	{
		RGB[0] = X;
		RGB[1] = C;
		RGB[2] = 0;
	}
	else if(HSL[0] >= 120 && HSL[0] < 180)
	{
		RGB[0] = 0;
		RGB[1] = C;
		RGB[2] = X;
	}
	else if(HSL[0] >= 180 && HSL[0] < 240)
	{
		RGB[0] = 0;
		RGB[1] = X;
		RGB[2] = C;
	}
	else if(HSL[0] >= 240 && HSL[0] < 300)
	{
		RGB[0] = X;
		RGB[1] = 0;
		RGB[2] = C;
	}else
	{
		RGB[0] = C;
		RGB[1] = 0;
		RGB[2] = X;
	}
	RGB[0] += m;
	RGB[1] += m;
	RGB[2] += m;
}

void computePointSaturationRegardingDistanceFromCylinderAxis(float *HSL, float distance, float existingSaturation)
{
	//linear function
	float slope = -strength / ray;
	float saturation = slope * (distance - ray);
	
	if((existingSaturation + saturation) >= 0 )
	{
		if((existingSaturation + saturation) <= strength)
			HSL[1] = existingSaturation + saturation;
		else
		{
			float dsat = strength - existingSaturation;
			 HSL[1] = existingSaturation + dsat;
		}

	}

}

void computeColorOfVertexInsideTheCylinderRegardingItsDistanceFromCylinderAxis(vector<float> &vertex_color_of_mesh_vertices,int indexToPositionsVectorVertexThatNeedsToBeColored, float distanceOfTheVertexThatNeedsToBeColoredFromCylinderAxis)
{
	int index = indexToPositionsVectorVertexThatNeedsToBeColored;
	float distance = distanceOfTheVertexThatNeedsToBeColoredFromCylinderAxis;
	
	//Step1 : find existing color of vertex and its saturation 
	float existingColorOfVertex[3];
	float existingSaturation;
		
	float currentColorInRGB[3] =  {vertex_color_of_mesh_vertices_ex2[index], vertex_color_of_mesh_vertices_ex2[index + 1], vertex_color_of_mesh_vertices_ex2[index + 2]};
	float hsl[3];
	RGBToHSL(hsl, currentColorInRGB);
	existingSaturation = hsl[1];


	//Step2 : convert from RGB to HSL
	float HSL[3];
	float RGB[3] = {BRUSH_R, BRUSH_G, BRUSH_B};
		
	//i do this transformation because i want to have hue and lightness of BRUSH_R, BRUSH_G, BRUSH_B in HSL
	RGBToHSL(HSL, RGB);

	//Step3 : compute point's saturation regarding its distance from cylinder axis
	//e.g.to points near the end of ray, saturation - > 0
		
	computePointSaturationRegardingDistanceFromCylinderAxis(HSL, distance, existingSaturation);

	//Step4 : transform from HSL to RGB
	HSLToRGB(RGB, HSL);

	//Step5 : change the RGB value in vertex_color_of_mesh_vertices_ex2 vector
			
	vertex_color_of_mesh_vertices_ex2[index] = RGB[0]; 
	vertex_color_of_mesh_vertices_ex2[index + 1] = RGB[1];
	vertex_color_of_mesh_vertices_ex2[index + 2] = RGB[2];

}

void computeTheColorOfVerticesInsideTheCylinderRegardingTheirDistanceFromCylinderAxis(vector<float> &vertex_color_of_mesh_vertices, vector<int> indexVectorToPositionsVectorVerticesInsideTheCylinder, vector<float> distanceVectorOfVerticesInsideTheCylinder, int opacityOfMesh, vector<bool> vertexInsideTheCylinderIsVisibleByCamera)
{
	vector<int> index = indexVectorToPositionsVectorVerticesInsideTheCylinder;
	vector<float> distance = distanceVectorOfVerticesInsideTheCylinder;

	if(!opacityOfMesh)
	{
		//change the color of all vertices inside the cylinder
		for(int i = 0 ; i < index.size() ; i++)	
			computeColorOfVertexInsideTheCylinderRegardingItsDistanceFromCylinderAxis(vertex_color_of_mesh_vertices, index[i],distance[i]);
	}
	else
	{
		for(int i = 0 ; i < indexVectorToPositionsVectorVerticesInsideTheCylinder.size() ; i++)
		{
			// check if the vertexInsideTheCylinder has to change color or not
			if(vertexInsideTheCylinderIsVisibleByCamera[i])
				computeColorOfVertexInsideTheCylinderRegardingItsDistanceFromCylinderAxis(vertex_color_of_mesh_vertices, index[i],distance[i]);		
		}
	}
}

void brush_3d()
{
	
	//Step1 : use ray cast picking and find if there is intersection pt
	//if not then nothing is brushed/done
	GLdouble vecBegin[3],vecEnd[3];
	float intersection_pt[3];
    int index_of_indices_vector_that_shows_the_intersected_tri = 0;
	if(rayCastPicking(vecBegin, vecEnd, index_of_indices_vector_that_shows_the_intersected_tri, intersection_pt, mx0, my0))
	{

		//Step2  : find the direction of 3d-brush - > it is represented by a cylinder with ray the given respective value by the user
		float directionOfCylinderAxis[3];
		defineCylinderThatRepresentsThe3dBrush(directionOfCylinderAxis, vecBegin, vecEnd);
		
		//Step3  : find the vertices of the mesh that are inside the cylinder
		vector<int> indexVectorToPositionsVectorVerticesInsideTheCylinder(0);
		vector<float> distanceVectorOfVerticesInsideTheCylinder(0);

		findVerticesOfMeshThatAreInsideTheCylinderAndSaveTheirDistanceFromCylinderAxis(vecBegin, vecEnd, ray, indexVectorToPositionsVectorVerticesInsideTheCylinder, distanceVectorOfVerticesInsideTheCylinder);
		
		//Step4 : check if opacity is one, if yes then from the vertices that are inside the cylinder find the ones that are visible by the camera 
		vector<bool> vertexInsideTheCylinderIsVisibleByCamera(0);
		if(opacity)
			findIfVerticesInsideTheCylinderAreVisibleByCamera(vertexInsideTheCylinderIsVisibleByCamera, vecBegin, vecEnd, directionOfCylinderAxis, indexVectorToPositionsVectorVerticesInsideTheCylinder);
		
		//Step5  : compute the color of vertices that are inside the cylinder and need to be colored
		computeTheColorOfVerticesInsideTheCylinderRegardingTheirDistanceFromCylinderAxis(vertex_color_of_mesh_vertices_ex2, indexVectorToPositionsVectorVerticesInsideTheCylinder, distanceVectorOfVerticesInsideTheCylinder, opacity, vertexInsideTheCylinderIsVisibleByCamera);
		
	}

}

void clearColorFromModel()
{
	vertex_color_of_mesh_vertices_ex2.clear();
	float color[3] = {0.8, 0.8, 0.8};
	initializeTheColorVectorOfMeshVertices(color,vertex_color_of_mesh_vertices_ex2);

}

/////////////////end of functions needed for 3D-brush//////////////////////////////

/////////////////functions needed for save/load of colored/deformated model////////

void saveModifiedModelToAFile(FILE *f, mesh_t meshObj, vector<float> color)
{
	//step1 : create file
	f = fopen("modified_model.txt", "w");
	if(f == NULL)																   	
	{																			   
		printf("Error opening modified_model file!\n");									   
		exit(1);																   
	}																			   
						
	//step2 : write vertices positions to the file
	vector<float> positions = meshObj.positions;
	for(int i = 0 ; i < positions.size() ; i += 3)
		fprintf(f, "vr%d %f %f %f\n", i / 3, positions[i], positions[i + 1], positions[i + 2]);
	
	//step3 : write normals of mesh vertices to the file
	vector<float> normals = meshObj.normals;
	for(int i = 0 ; i < normals.size() ; i += 3)
		fprintf(f, "vn%d %f %f %f\n", i / 3, normals[i], normals[i + 1], normals[i + 2]);

	//step4 : write faces, each of which corresponds to indices of a triangle's vertices(in counterclockwise order)  
	vector<unsigned int> indices = meshObj.indices;
	for(int i = 0 ; i < indices.size() ; i += 3)
		fprintf(f, "fc%d %f %f %f\n", i / 3, (float)indices[i], (float)indices[i + 1], (float)indices[i + 2]);

	//step5 : write color of each vertex of the mesh to the file				   					   
	for(int i = 0 ; i < vertex_color_of_mesh_vertices_ex2.size() ; i += 3)		   
		fprintf(f, "vc%d %f %f %f\n", i/3, color[i], color[i + 1], color[i + 2]);   
																				   
	//step3 : close file														    
	fclose(f);																	    
																					
}																				   

void loadModifiedModel(FILE *f, mesh_t &meshObj, vector<float> &colorVector)													   
{																				   
	//step1 : open the file														   
	f = fopen("modified_model.txt", "r");											    
																				    
	if(f == NULL)																    
	{
		printf("Error opening color file!\n");
		exit(1);																    
	}																			    
																				    
	//step2 : read from the file	
	char n1 ,n2 ;
	int n = 0;
	float num[4] = {0,0,0,0};
	while(fscanf(f, "%c%c%d %f %f %f\n", &n1, &n2, &n, &num[0], &num[1], &num[2]) == 6)
	{
		if(n1 == 'v' && n2 == 'r')
		{
			meshObj.positions.push_back(num[0]);
			meshObj.positions.push_back(num[1]);
			meshObj.positions.push_back(num[2]);
		}

		if(n1 == 'v' && n2 == 'n')
		{
			meshObj.normals.push_back(num[0]);
			meshObj.normals.push_back(num[1]);
			meshObj.normals.push_back(num[2]);
		}

		if(n1 == 'f')
		{
			meshObj.indices.push_back((int)num[0]);
			meshObj.indices.push_back((int)num[1]);
			meshObj.indices.push_back((int)num[2]);
		}

		if(n1 == 'v' && n2 == 'c')
		{
			colorVector.push_back(num[0]);
			colorVector.push_back(num[1]);
			colorVector.push_back(num[2]);
		}

		
	}


	if(feof(f))
		cout << endl << "reached end of file";
	else
		cout << endl << "some error interrupted the read";
	
	

	//step4 : close file
	fclose(f);
}
/////////////////end of functions needed for save/load of colored/deformated model////////////


/////////////////functions needed for modified brush-3d/////////////////////

void initializeWeightVectorOfMeshVertices()
{
	for(int i = 0 ; i < mesh.positions.size() ; i += 3)
		weightOfEveryVertexOfMesh.push_back(0);
	
}

void computeWeightOfVertexInsideTheCylinderThatNeedsToBeBrushedRegardingItsDistanceFromCylinderAxis(vector<float> &weightVectorOfMeshVertices, float distanceOfMeshVertexInsideTheCylider, int indexToPositionsVectorVertexInsideTheCylinderThatNeedsToChangeItsWeight)
{
	//Step1 : compute point's weight regarding its distance from cylinder axis
	//e.g.to points near the end of ray, weight - > 0
	float distance = distanceOfMeshVertexInsideTheCylider;
	int index = indexToPositionsVectorVertexInsideTheCylinderThatNeedsToChangeItsWeight;
	
	float weight;
	float existingWeight = weightVectorOfMeshVertices[index / 3];
			
	//linear function
	float slope = -strength / ray;
	weight = slope * (distance - ray);
	
	if((existingWeight + weight) <= strength)
		weight = existingWeight + weight;
	else
	{
		float dsat = strength - existingWeight;
		weight = existingWeight + dsat;
	}

	//Step3 : update data to weightofEveryVertexOfMesh  
	weightVectorOfMeshVertices[index / 3] = weight;
			
}

void computeWeightOfVerticesInsideTheCylinderRegardingTheirDistanceFromCylinderAxis(vector<int> indexVectorToPositionsVectorVerticesInsideTheCylinder, int opacityOfMesh, vector<bool> vertexInsideTheCylinderIsVisibleByCamera, vector<float> distanceVectorOfVerticesInsideTheCylinder)
{
	vector<int> index = indexVectorToPositionsVectorVerticesInsideTheCylinder;
	vector<float> distance = distanceVectorOfVerticesInsideTheCylinder;
	
	//Step1 : if opacity is 1 then compute weights for all vertices inside the cylinder, else compute weight of the visible ones by camera only
	if(!opacityOfMesh)
	{

		for(int i = 0 ; i < index.size() ; i++)
			computeWeightOfVertexInsideTheCylinderThatNeedsToBeBrushedRegardingItsDistanceFromCylinderAxis(weightOfEveryVertexOfMesh, distance[i], index[i]);
	}
	else
	{
		for(int i = 0 ; i < index.size() ; i++)
		{	
			if(vertexInsideTheCylinderIsVisibleByCamera[i])
				computeWeightOfVertexInsideTheCylinderThatNeedsToBeBrushedRegardingItsDistanceFromCylinderAxis(weightOfEveryVertexOfMesh, distance[i], index[i]);
		}
	}
}

void computeColorOfVertexInsideTheCylinderRegardingItsWeight(vector<float> &vertex_color_of_mesh_vertices, int indexToPositionsVectorVertexThatNeedsToBeColored, float weightOfVertexThatNeedsToBeColored)
{
	int index = indexToPositionsVectorVertexThatNeedsToBeColored;
	float weight = weightOfVertexThatNeedsToBeColored;
	
	//if weight -> 0, then the color of vertex is going to be black
	//in order to avoid it I change the red value to 0.05
	if(weight > 0.1)
		vertex_color_of_mesh_vertices[index] = 1 * weight;  
	else
		vertex_color_of_mesh_vertices[index] = 0.1;
	vertex_color_of_mesh_vertices[index + 1] = 0;
	vertex_color_of_mesh_vertices[index + 2] = 0;

}

void computeColorOfVerticesInsideTheCylinderRegardingTheirWeights(vector<float> &vertex_color_of_mesh_vertices, vector<int> indexVectorToPositionsVectorVerticesInsideTheCylinder, vector<float> weigthsOfMeshVertices, int opacityOfMesh, vector<bool> vertexInsideTheCylinderIsVisibleByCamera)
{
	vector<int> index = indexVectorToPositionsVectorVerticesInsideTheCylinder;
	vector<float> weight = weigthsOfMeshVertices;
	
	//Step1 : if opacity is 1 then compute colors for all vertices inside the cylinder, else compute color of the visible vertices inside the cylinder by camera only
	if(!opacityOfMesh)
	{
		for(int i = 0 ; i < index.size() ; i ++)
			computeColorOfVertexInsideTheCylinderRegardingItsWeight(vertex_color_of_mesh_vertices, index[i], weight[index[i] / 3]);
	
	}
	else
	{
		for(int i = 0 ; i < index.size() ; i++)
		{
			if(vertexInsideTheCylinderIsVisibleByCamera[i])
				computeColorOfVertexInsideTheCylinderRegardingItsWeight(vertex_color_of_mesh_vertices, index[i], weight[index[i] / 3]);
		}
	}
}



void brush3Dmodified()
{
	//step1 : use ray cast picking and find if there is intersection pt
	//if not then nothing is brushed/done
	GLdouble vecBegin[3],vecEnd[3];
	float intersection_pt[3];
    int index_of_indices_vector_that_shows_the_intersected_tri;
	if(rayCastPicking(vecBegin, vecEnd, index_of_indices_vector_that_shows_the_intersected_tri, intersection_pt, mx0, my0))
	{

		//step2  : find the direction of 3d-brush - > it is represented by a cylinder with ray the given equivalent value by the user
		float directionOfCylinderAxis[3];
		defineCylinderThatRepresentsThe3dBrush(directionOfCylinderAxis, vecBegin, vecEnd);
		
		//step3  : find the vertices of the mesh that are inside the cylinder
		vector<int> indexVectorToPositionsVectorVerticesInsideTheCylinder(0);
		vector<float> distanceVectorOfVerticesInsideTheCylinder(0);

		findVerticesOfMeshThatAreInsideTheCylinderAndSaveTheirDistanceFromCylinderAxis(vecBegin, vecEnd, ray, indexVectorToPositionsVectorVerticesInsideTheCylinder, distanceVectorOfVerticesInsideTheCylinder);
		

		//step4 : if opacity is 1, then from the vertices that are inside the cylinder find the ones that are visible by the camera
		vector<bool> vertexInsideTheCylinderIsVisibleByCamera(0);
		
		if(opacity)
			findIfVerticesInsideTheCylinderAreVisibleByCamera(vertexInsideTheCylinderIsVisibleByCamera, vecBegin, vecEnd, directionOfCylinderAxis, indexVectorToPositionsVectorVerticesInsideTheCylinder);

		//step5 : compute the weight of vertices that are inside the cylinder 
		computeWeightOfVerticesInsideTheCylinderRegardingTheirDistanceFromCylinderAxis(indexVectorToPositionsVectorVerticesInsideTheCylinder, opacity, vertexInsideTheCylinderIsVisibleByCamera, distanceVectorOfVerticesInsideTheCylinder);
		
		//step6 : compute color of vertices inside the cylinder regarding their weights
		computeColorOfVerticesInsideTheCylinderRegardingTheirWeights(vertex_color_of_mesh_vertices_ex2, indexVectorToPositionsVectorVerticesInsideTheCylinder, weightOfEveryVertexOfMesh, opacity, vertexInsideTheCylinderIsVisibleByCamera);
	}

}

/////////////////end of functions needed for modified brush-3d//////////////


/////////////////functions needed for anchor point//////////////////////////
void selectAnchorPoint()
{
	//Step1 : Ray Cast-Picking
	GLdouble vecBegin[3], vecEnd[3];
	int indexOfIndicesVectorThatShowsIntersectedTri;
	float intersectionPt[3];
	
	
	bool intersection = false;
	if(mouseClickForRayCastCheck)
	{
		intersection = rayCastPicking(vecBegin, vecEnd, indexOfIndicesVectorThatShowsIntersectedTri, intersectionPt, mx0, my0);
		
		if(!intersection)
		{
			cout << endl << "The selected anchor point was not a point of the mesh" <<endl;
			cout << "Click again" << endl;
		}
		mouseClickForRayCastCheck = false;
	}
		
	//Step2 : Save to global variable the selected anchor point
	if(intersection)
	{	
		anchorPointExists = true;
		anchorPoint[0] = intersectionPt[0];
		anchorPoint[1] = intersectionPt[1];
		anchorPoint[2] = intersectionPt[2];
	}
}

void visualizeAnchorPointWithASphere()
{
	glPushMatrix();
	glColor3f(0.8, 0.8, 0.8);
	glTranslatef(anchorPoint[0], anchorPoint[1], anchorPoint[2]);
	glutSolidSphere(3, 20, 20);
	glPopMatrix();

}

/////////////////end of functions needed for anchor point///////////////////


/////////////////functions needed for simple deformation tool///////////////
void findEndPointOfDirectionVectorOfUsersDrag(float *intersection_pt2, float *intersection_pt1, GLdouble *vecBegin2, GLdouble *vecEnd2)
{
	float a[4];
	float vert1[3] = {0, 0, intersection_pt1[2]}, vert2[3] = {1, 0, intersection_pt1[2]}, vert3[3] = {0, 1, intersection_pt1[2]};
	PlaneEquation1(a, a + 1, a + 2, a + 3, vert1, vert2 , vert3);
	float intersection_pt[3];
	IntersectionPointOfPlaneAndRay3D(intersection_pt2, vecBegin2, vecEnd2, vecBegin2, a[0], a[1], a[2], a[3]);

}

void findDirectionOfDeformation(float *direction ,float *ptBegin, float *ptEnd)
{
	float dir[3] = {ptEnd[0] - ptBegin[0], ptEnd[1] - ptBegin[1], ptEnd[2] - ptBegin[2]};
	float length = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));

	direction[0] = dir[0] / length;
	direction[1] = dir[1] / length;
	direction[2] = dir[2] / length;
}


void findPlaneEquation2(float &a, float &b, float &c, float &d, float *directionOfPlane, float *ptOfPlane)
{
	a = directionOfPlane[0];
	b = directionOfPlane[1];
	c = directionOfPlane[2];

	d = - a * ptOfPlane[0] - b * ptOfPlane[1] - c * ptOfPlane[2];  
}

bool PointIsAboveThePerpendicularToDragPlaneThatContainsAnchorPoint(float *pt, float *anchor_pt,float a, float b, float c, float d)
{
	if(abs(b) > 0.05)
	{
		float y = (-a * pt[0] - c * pt[2] - d) / b;
		if(y > pt[1])
			return false;
		else 
			return true;
	}
	else
	{
		//plane is horizontal 
		if(pt[1] >= anchor_pt[1])
			return true;
		else return false;
	}
}

void findSetOfMeshVerticesThatAreOnTheSameSideOfPlaneAsIntersectionPt1(vector<int> &indexVectorToPositionsVectorForVerticesOnTheSameSideOfPlaneAsTheFirstClickedPoint , mesh_t meshObj,int above, float *anchor_pt, float a, float b, float c, float d)
{
	vector<int> &index = indexVectorToPositionsVectorForVerticesOnTheSameSideOfPlaneAsTheFirstClickedPoint;
	vector<float> positions = meshObj.positions;

	for(int i = 0 ; i < positions.size() ; i += 3)
	{
		float pt[3] = {positions[i], positions[i + 1], positions[i + 2]};
		if(above){
			if(PointIsAboveThePerpendicularToDragPlaneThatContainsAnchorPoint(pt,anchor_pt, a, b, c, d))
			{
				index.push_back(i);
			}
		}
		else
		{
			if(!PointIsAboveThePerpendicularToDragPlaneThatContainsAnchorPoint(pt,anchor_pt, a, b, c, d))
			{
				index.push_back(i);
			}
		}
	}
}



void computeNewPositionsDueToDeformation(mesh_t &meshObj, mesh_t meshInit,float distance, float *direction,vector<int> indexToPositionsVectorForVerticesOfMeshThatNeedToBeTranslated, vector<float> weightVector)
{
	vector<int> index = indexToPositionsVectorForVerticesOfMeshThatNeedToBeTranslated;
	vector<float> &positions = meshObj.positions;
	vector<float> initialPositions = meshInit.positions;
	//I assume that line3d equation is line = pt + l * direction
	//the length of the vector to add to pt is w * distance
	//so i conclude that l = w * distance
	for(int i = 0 ; i < index.size() ; i++)
	{
		positions[index[i]] = weightVector[index[i]/3] * distance * direction[0] + initialPositions[index[i]];
		positions[index[i] + 1] = weightVector[index[i]/3] * distance * direction[1] + initialPositions[index[i] + 1];
		positions[index[i] + 2] = weightVector[index[i]/3] * distance * direction[2] + initialPositions[index[i] + 2];
	}

}


void simpleDeformationTool(vector<int> mouseClickCoordsVector, int screen_coord_of_end_pt_x, int screen_coord_of_end_pt_y)
{
	

	if(anchorPointExists)
	{
		//step1 : process time begins
		if(!timeNeededForSimpleDeformation)
		{
				timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
				previousSinceStartTime = timeSinceStart;
		}

		//Step2 : given the 2  screen coord points/pairs, find their world coord points respectively(intersection_pt1, intersection_pt2)
		GLdouble vecBegin1[3], vecEnd1[3], vecBegin2[3], vecEnd2[3];
		get3DRay(vecBegin1, vecEnd1, mouseClickCoordsVector[0], mouseClickCoordsVector[1]);
		get3DRay(vecBegin2, vecEnd2, mx0, my0);
	
		//for the first point, check if it intersects with mesh and if yes, then find the intersection_pt1
		int index_to_indices_vector_of_intersected_tri;
		float intersection_pt1[3], intersection_pt2[3];
		if(rayCastPicking(vecBegin1, vecEnd1, index_to_indices_vector_of_intersected_tri, intersection_pt1, mouseClickCoordsVector[0], mouseClickCoordsVector[1]))
		{
			//find the intersection pt of the ray3d (defined from vecBegin2,vecEnd2) and the plane z = intersection_pt1[2]
			//intersection_pt2 :
			findEndPointOfDirectionVectorOfUsersDrag(intersection_pt2, intersection_pt1, vecBegin2, vecEnd2);

			//draw direction line so as to check everything is programmed properly
			glPushMatrix();
			glBegin(GL_LINES);
			glColor3f(1, 0, 0);
			glVertex3fv(intersection_pt1);
			glVertex3fv(intersection_pt2);
			glEnd();
			glPopMatrix();

			//Step3 : find direction of deformation
			float direction[3];
			findDirectionOfDeformation(direction, intersection_pt1, intersection_pt2);

			//Step4 : find the plane equation of the plane that has normal the same as the drag's direction vector and includes the anchor point
			float a[4]; 
			findPlaneEquation2(a[0], a[1], a[2], a[3], direction, anchorPoint);

			//Step5 : find the length of max deformation
			float distance = sqrt(pow(intersection_pt2[0] - intersection_pt1[0], 2)+ pow(intersection_pt2[1] - intersection_pt1[1], 2) + pow(intersection_pt2[2] - intersection_pt1[2], 2));

			
			//Step6 : find if intersection_pt1 is up or down the above defined plane
			//define above - i assume that if the firstClickOfDragShowsToAPointAboveThePerpendicularToDragPlaneThatContainsAnchorPoint is true,then above == 1
			//else above = 0
			int above ; 
			if(PointIsAboveThePerpendicularToDragPlaneThatContainsAnchorPoint(intersection_pt1, anchorPoint, a[0], a[1], a[2], a[3]))	
				above = 1;
			else
				above = 0;

			//Step7 : find the set of mesh vertices that are on the same side with the intersection_pt1 relatively to the horizontal plane
			vector<int> indexVectorToPositionsVectorForVerticesOnTheSameSideOfPlaneAsTheFirstClickedPoint(0);
			vector<int> &index = indexVectorToPositionsVectorForVerticesOnTheSameSideOfPlaneAsTheFirstClickedPoint;
			findSetOfMeshVerticesThatAreOnTheSameSideOfPlaneAsIntersectionPt1(index, mesh, above, anchorPoint, a[0], a[1], a[2], a[3]);
			

			//Step8 : for this set of vertices, compute their new position/deformation regarding their weight
			computeNewPositionsDueToDeformation(mesh, meshInitial, distance, direction, index, weightOfEveryVertexOfMesh);
			
		}
		else
		{
			cout << endl << "There was no click on the mesh!";
			cout << endl << "The first click must be on the mesh, so as to activate deformation tool.";
			cout << endl << "Click again...";
		}
	}
	else
	{
		cout << endl <<"No anchor point is selected!";
		cout << endl << "First choose an anchor point and then try deformation again ;) .";
	}
	
	//step9 : process time ends
	timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
	timeNeededForSimpleDeformation = timeSinceStart - previousSinceStartTime;
	cout << endl << "Process Time for simple deformation(in sec) : " << (float) timeNeededForSimpleDeformation/1000 ;
	timeNeededForSimpleDeformation = 0;
}


/////////////////end of functions needed for simple deformation tool//////////////////////////////


/////////////////functions needed for handle deformation of non-overlapped brushed regions////////

bool elementAlreadyExistsToVector(vector<int> vector, int element)
{
	for(int a = 0 ; a < vector.size() ; a++)
	{
		if(vector[a] == element)
			return true;
	}
	return false;

}

bool triAlreadyExistsToTrisBrushedVector(vector<int> indexVectorToIndicesVectorToTrisBrushedByModifiedBrush3D, int index)
{
	if(elementAlreadyExistsToVector(indexVectorToIndicesVectorToTrisBrushedByModifiedBrush3D, index))
		return true;
	return false;
}

void findTrianglesThatHaveBeenBrushedByTheModifiedBrush3D(vector<int> &indexVectorToIndicesVectorToTrisBrushedByModifiedBrush3D, vector<float> weightVector, mesh_t meshObj)
{
	vector<unsigned int> indices = meshObj.indices;

	for(int i = 0 ; i < weightVector.size() ; i++)
	{
		//Step1 : check if weight is non-zero 
		if(weightVector[i] > 0)
		{
			for(int j = 0 ; j < indices.size() ; j += 3)
			{
				if((indices[j] - i) < 0.01 || indices[j + 1] - i < 0.01 || indices[j + 2] - i < 0.01)
				{
					if(!triAlreadyExistsToTrisBrushedVector(indexVectorToIndicesVectorToTrisBrushedByModifiedBrush3D, j))
					{
						indexVectorToIndicesVectorToTrisBrushedByModifiedBrush3D.push_back(j);
					}
				}
			
			}
		}
	}

}

void checkIfAnyOfTheOther2VerticesOfTriNeedToBeTranslated(vector<int> &indexToWeightVectorToVerticesNeedToBeTranslated, vector<float> weightOfMeshVertices, int indexToVert1, int indexToVert2)
{
	if(weightOfMeshVertices[indexToVert1] > 0)
	{
		if(!elementAlreadyExistsToVector(indexToWeightVectorToVerticesNeedToBeTranslated,indexToVert1))
		{
			indexToWeightVectorToVerticesNeedToBeTranslated.push_back(indexToVert1);
		}
	}	
	if(weightOfMeshVertices[indexToVert2] > 0)
	{
		if(!elementAlreadyExistsToVector(indexToWeightVectorToVerticesNeedToBeTranslated,indexToVert2))
		{
			indexToWeightVectorToVerticesNeedToBeTranslated.push_back(indexToVert2);
		}
	}	

}

void findAllTheTrisThatBelongToTheSameBrushedRegionAsTheClickedTriangle(vector<int> &indexToWeightVectorToVerticesNeedToBeTranslated, vector<float> weightOfMeshVertices,vector<int> indexVectorToIndicesVectorToBrushedTris, mesh_t meshObj)
{
	vector<unsigned int> indices = meshObj.indices;
	vector<int> &index = indexToWeightVectorToVerticesNeedToBeTranslated;
	for(int l = 0 ; l < index.size() ; l++)
	{
		//for each element of indexToWeightVectorToVerticesNeedToBeTranslated 
		//check for any colored triangle if the point with index <<l>> is vertex of it 
		//if yes check if the other two vertices of tri have weight != 0 and if yes 
		//check if they don't already exist and push them back to indexToWeightVectorToVerticesNeedToBeTranslated
		for(int i = 0 ; i < indexVectorToIndicesVectorToBrushedTris.size() ; i++)
		{
			//indices to vertices of the triangle
			int ind1 = indices[indexVectorToIndicesVectorToBrushedTris[i]];
			int ind2 = indices[indexVectorToIndicesVectorToBrushedTris[i] + 1];
			int ind3 = indices[indexVectorToIndicesVectorToBrushedTris[i] + 2];

			if(ind1 == l)
			{
				checkIfAnyOfTheOther2VerticesOfTriNeedToBeTranslated(indexToWeightVectorToVerticesNeedToBeTranslated, weightOfMeshVertices, ind2, ind3);
			}
			if(ind2 == l)
			{
				checkIfAnyOfTheOther2VerticesOfTriNeedToBeTranslated(indexToWeightVectorToVerticesNeedToBeTranslated, weightOfMeshVertices, ind1, ind3);
			}
			if(ind3 == l)
			{
				checkIfAnyOfTheOther2VerticesOfTriNeedToBeTranslated(indexToWeightVectorToVerticesNeedToBeTranslated, weightOfMeshVertices, ind1, ind2);
			}
		}
	}
}

void handleDeformationOfNonOverlappedBrushedRegionsByBrush3D(vector<float> weightOfMeshVertices, vector<int> indexVectorToIndicesVectorToBrushedTris, mesh_t meshObj)
{
	//Step1 : find if the ray3d intersected a triangle of the mesh
	//if no then do nothing
	GLdouble vecBegin[3], vecEnd[3];
	int index_of_indices_vector_to_the_intersected_tri;
	float intersection_pt[3];
	
	if(rayCastPicking(vecBegin, vecEnd, index_of_indices_vector_to_the_intersected_tri, intersection_pt, mouseClickCoordsVector[0], mouseClickCoordsVector[1]))
	{
		
		//Step2 : if yes check if any of its vertices has non-zero weight
		vector<int> indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated(0);
		vector<int> indexVectorToIndicesVectorToTrisThatNeedToBeDeformated(0);
		int index = index_of_indices_vector_to_the_intersected_tri;
		vector<unsigned int> indices = mesh.indices;
		if((weightOfEveryVertexOfMesh[indices[index]] > 0.01))
			indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated.push_back(indices[index]);
		if((weightOfEveryVertexOfMesh[indices[index + 1]] > 0.01))
			indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated.push_back(indices[index + 1]);
		if((weightOfEveryVertexOfMesh[indices[index + 2]] > 0.01))
			indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated.push_back(indices[index + 2]);
		
		//step3 : find all the tris that belong to the same brushed region as the clicked triangle
		if(!indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated.empty())
		{
			indexVectorToIndicesVectorToTrisThatNeedToBeDeformated.push_back(index_of_indices_vector_to_the_intersected_tri);
			findAllTheTrisThatBelongToTheSameBrushedRegionAsTheClickedTriangle(indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated, weightOfMeshVertices, indexVectorToIndicesVectorToBrushedTris, meshObj);
			//step4 : color them magenta just to check the result and then delete
			for(int m = 0 ; m < indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated.size() ; m++)
			{
				vertex_color_of_mesh_vertices_ex2[3 * indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated[m]] = 1;
				vertex_color_of_mesh_vertices_ex2[3 * indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated[m] + 1] = 0;
				vertex_color_of_mesh_vertices_ex2[3 * indexVectorToWeightVectorToColoredVerticesThatNeedToBeTranslated[m] + 2] = 1;
			}
		}

	
	}
	else
	{
		cout << endl << " No click on the mesh! " ;
		cout << endl << " To activate <<handle deformation of non overlapped brushed regions tool>> you have to click on the mesh. " ;
	}

}
/////////////////end of functions needed for handle deformation of non-overlapped brushed regions/

/*******************************************************************************
  Implementation of Visual Functions
 ******************************************************************************/

void setup()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
    
    glShadeModel(GL_SMOOTH);

    
    glEnable(GL_POINT_SMOOTH);
  
   
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    glEnable(GL_DEPTH_TEST);
    
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1);
  
   
    //glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT_AND_BACK);
	glFrontFace(GL_CCW);
	

    glEnable(GL_COLOR_MATERIAL);
    
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

   
    glEnable(GL_LIGHTING);

 
    GLfloat lightAmbient[]   = {0.2,   0.2,   0.2, 1.0};
    GLfloat lightDiffuse[]   = {0.6,   0.6,   0.6, 1.0};
    GLfloat lightSpecular[]  = {0.9,   0.9,   0.9, 1.0};
    GLfloat lightPosition[]  = {0.0,   0.0,   0.0, 1.0};
    GLfloat lightDirection[] = {0.0,   0.0,  -1.0, 0.0};
  
   
    glLightfv(GL_LIGHT0, GL_AMBIENT,  lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  lightDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);   
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
  
    
    glEnable(GL_LIGHT0);
    
   

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	readObj(getExeDir() + "..\\Models\\tyra_norm.obj", mesh);
	//readObj(getExeDir() + "..\\Models\\f16.obj", mesh);
	//readObj(getExeDir() + "..\\Models\\camel.obj", mesh);
	//readObj(getExeDir() + "..\\Models\\headCry.obj", mesh);

	//initialize the color vector of each vertex of mesh
	float color[3] = {0.8, 0.8, 0.8};
	initializeTheColorVectorOfMeshVertices(color, vertex_color_of_mesh_vertices_ex1);
	initializeTheColorVectorOfMeshVertices(color, vertex_color_of_mesh_vertices_ex2);

	//initialize weight of mesh's vertices
	initializeWeightVectorOfMeshVertices();

	//copy the initial mesh to meshInitial
	meshInitial = mesh;
}



void render()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	
    gluLookAt(cx, cy, cz,  
               0,  0,  0, 
               0,  1,  0); 
	
   
    glPushMatrix();
    glTranslatef(dx, dy, dz);
    glRotatef(ry, 0, 1, 0);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);



	//1rst question : ray-cast picking
	if(mode & EX1A)
	{
		drawObj(mesh, vertex_color_of_mesh_vertices_ex1);
		if(checkForCollision)
		{
			highLightPickedTri(mesh);	
	
			checkForCollision = false;
		}
	}

	//2nd question of partA : 3d-brush
	if(mode & EX2A)
	{
		drawObj(mesh, vertex_color_of_mesh_vertices_ex2);

		//step1 : define attributes of brush
		if(newBrush)
		{
			askTheUserToEnterTheAttributesOf3dBrusher(ray, strength, opacity);
			float color[3] = {0.8 , 0.8, 0.8};
			//clear the vectors used while brush
			vertex_color_of_mesh_vertices_ex2.clear();
			initializeTheColorVectorOfMeshVertices(color,vertex_color_of_mesh_vertices_ex2);
			mesh = meshInitial; 
			newBrush = false;
		}
		
		if(brushOn)
		{
			brush_3d();
	
			brushOn = false;
		}
		
		//3rd question of partA : save-load colored file
		FILE f ;
		if(saveModifiedModelToFile)
		{
			cout << endl << "Save Modified Model..." << endl;
			saveModifiedModelToAFile(&f, mesh, vertex_color_of_mesh_vertices_ex2);
			saveModifiedModelToFile = false;
		}
		if(loadModifiedModelFromFile)
		{
			cout << endl << "Read Model's info from 'modified_model.txt' and load it..." << endl;
			mesh.indices.clear();
			mesh.normals.clear();
			mesh.positions.clear();
			vertex_color_of_mesh_vertices_ex2.clear();
			loadModifiedModel(&f, mesh, vertex_color_of_mesh_vertices_ex2);
			loadModifiedModelFromFile = false;
		
		}

		if(clearColorMadeByBrush)
		{
			clearColorFromModel();
			clearColorMadeByBrush = false;
		}
	}

	//1rst question of partB : modified brush-3d
	if(mode & EX1B)
	{
		drawObj(mesh, vertex_color_of_mesh_vertices_ex2);
		//step1 : define attributes of brush
		if(newBrush)
		{

			askTheUserToEnterTheAttributesOf3dBrusher(ray, strength, opacity);
			float color[3] = {0.8 , 0.8, 0.8};
			//clear the vectors used while brush
			vertex_color_of_mesh_vertices_ex2.clear();
			initializeTheColorVectorOfMeshVertices(color,vertex_color_of_mesh_vertices_ex2);
			weightOfEveryVertexOfMesh.clear();
			initializeWeightVectorOfMeshVertices();
			mesh = meshInitial;
			newBrush = false;
		}
		
		if(brushOn)
		{
			brush3Dmodified();
	
			brushOn = false;
		}
		
		//3rd question of partA : save-load colored file
		FILE f ;
		if(saveModifiedModelToFile)
		{
			cout << endl << "Save Modified Model..." << endl;
			saveModifiedModelToAFile(&f, mesh, vertex_color_of_mesh_vertices_ex2);
			saveModifiedModelToFile = false;
		}

		if(loadModifiedModelFromFile)
		{
			cout << endl << "Read Model's info from 'modified_model.txt' and load it..." << endl;
			mesh.indices.clear();
			mesh.normals.clear();
			mesh.positions.clear();
			vertex_color_of_mesh_vertices_ex2.clear();
			loadModifiedModel(&f, mesh, vertex_color_of_mesh_vertices_ex2);
			loadModifiedModelFromFile = false;
		
		}

		if(clearColorMadeByBrush)
		{
			clearColorFromModel();
			clearColorMadeByBrush = false;
		}
	}

	//2nd question of partB : select anchor point
	if(mode & EX2B)
	{
		drawObj(mesh, vertex_color_of_mesh_vertices_ex1);
		if(selectAnchorPointOn)
		{
			selectAnchorPoint();
			
			selectAnchorPointOn = false;
		}
		if(anchorPointExists)
			visualizeAnchorPointWithASphere();
	}

	//3rd question of partB : simple deformation
	if(mode & EX3B)
	{
		
		drawObj(mesh,vertex_color_of_mesh_vertices_ex1);
		
		if(anchorPointExists)
			visualizeAnchorPointWithASphere();


		if(!cameraOn && deformationOn)
		{
			
			
				if(mouseClickCoordsVector.size() >= 2 )
				{
					if((mouseClickCoordsVector[0] != mx0) && (mouseClickCoordsVector[1] != my0))
					{
						simpleDeformationTool(mouseClickCoordsVector, mx0, my0);
					}
				}
				
				deformationOn = false;
		}
		
		//5th question of partB : save/load modified model
		FILE f ;
		if(saveModifiedModelToFile)
		{
			cout << endl << "Save Modified Model..." << endl;
			saveModifiedModelToAFile(&f, mesh, vertex_color_of_mesh_vertices_ex1);
			saveModifiedModelToFile = false;
		}
		if(loadModifiedModelFromFile)
		{
			cout << endl << "Read Model's info from 'modified_model.txt' and load it..." << endl;
			mesh.indices.clear();
			mesh.normals.clear();
			mesh.positions.clear();
			vertex_color_of_mesh_vertices_ex1.clear();
			loadModifiedModel(&f, mesh, vertex_color_of_mesh_vertices_ex1);
			loadModifiedModelFromFile = false;
		
		}
	
	}

	if(mode & EX4B)
	{
		
		drawObj(mesh,vertex_color_of_mesh_vertices_ex2);
		
		if(handleDeformationOfNonOverlappedBrushedRegions)
		{
			
			handleDeformationOfNonOverlappedBrushedRegionsByBrush3D(weightOfEveryVertexOfMesh, indexToIndicesVectorToTrisBrushedByBrush3D, mesh);
		
			handleDeformationOfNonOverlappedBrushedRegions = false;
		}
		
	}


	glPopMatrix();
    glutSwapBuffers();
}

void resize(int w, int h)
{
    glMatrixMode(GL_PROJECTION); 
    // define the visible area of the window ( in pixels )
    if (h==0) h=1;
    glViewport(0, 0, w, h); 

    // Setup viewing volume
    glLoadIdentity();
 
    gluPerspective(60.0, (float)w/(float)h, 1.0, 500.0);
}



void idle()
{
    //glMatrixMode(GL_MODELVIEW);
    // Make your changes here
  
    // After your changes rerender
    //glutPostRedisplay();
}


void menuSelect(int choice)
{
    switch (choice)
    {
    case EX1A:
        mode ^= EX1A;
        break;
    case EX2A:
        mode ^= EX2A;
		if(mode & EX2A)
			newBrush = true;
		break;
	case EX1B:
		mode ^= EX1B;
		if(mode & EX1B)
			newBrush = true;//////i use the same flag for new brush of brush-3d and modified brush-3d
		break;
	case EX2B:
		mode ^= EX2B;
		if(!(mode & EX2B))
			mouseClickCoordsVector.clear();
		break;
	case EX3B:
		mode ^= EX3B;
		if(mode & EX3B)
			mouseClickCoordsVector.clear();
		break;
	case EX4B:
		mode ^= EX4B;
		if(mode & EX4B)
		{
			indexToIndicesVectorToTrisBrushedByBrush3D.clear();
			findTrianglesThatHaveBeenBrushedByTheModifiedBrush3D(indexToIndicesVectorToTrisBrushedByBrush3D, weightOfEveryVertexOfMesh, mesh);
		}
	default:
        break;
    }
    glutPostRedisplay();
}


void keyboardDown(unsigned char key, int x, int y)
{
    switch(key)
    {
	case 'X':
	case 'x':
		break;
	case 'Y':
	case 'y':
		break;
	case 'Z':
	case 'z':
		break;
    case 'W':
    case 'w':
        break;
    case 'A':
    case 'a':
        break;
    case 'S':
    case 's':
        break;
    case 'D':
    case 'd':
		cameraOn = false;
		deformationOn = true;
        break;
	case 'C':
	case 'c':
		clearColorMadeByBrush = true;
		cameraOn = true;
		deformationOn = false;
		break;
    case 'I':
    case 'i':
		dz -= 1;
        break;
    case 'K':
    case 'k':
		dz += 1;
        break;
    case 'J':
    case 'j':
		dx -= 1;
        break;
    case 'L':
    case 'l':
		dx += 1;
        break;
	case 'M':
	case 'm':
		break;
	case 'T':
	case 't':
		break;
    case '+':
        cdist -= zoomStep;
        cx = cdist*sin(altitudeAngle)*sin(azimuthAngle);
        cy = cdist*cos(altitudeAngle);
        cz = cdist*sin(altitudeAngle)*cos(azimuthAngle);
        break;
    case '-':
        cdist += zoomStep;
        cx = cdist*sin(altitudeAngle)*sin(azimuthAngle);
        cy = cdist*cos(altitudeAngle);
        cz = cdist*sin(altitudeAngle)*cos(azimuthAngle);
        break;
    case KEY_ESC:
        exit(0);
        break;
    default:
        break;
    }
    glutPostRedisplay();
}

void keyboardUp(unsigned char key, int x, int y)
{
	switch(key){
	case 'X':
	case 'x':
		break;
	case 'Y':
	case 'y':
		break;
	case 'Z':
	case 'z':
		break;
	case 'M':
	case 'm':
		break;
	case 'T':
	case 't':
		break;
	}

	glutPostRedisplay();
}



void keyboardSpecialDown(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_UP:
        dy += 1;
        break;
    case GLUT_KEY_DOWN:
        dy -= 1;
        break;
    case GLUT_KEY_RIGHT:
        ry -= 1;
        break;
    case GLUT_KEY_LEFT:
        ry += 1;
        break;
	case  GLUT_KEY_F1:
		saveModifiedModelToFile = true;
		break;
	case  GLUT_KEY_F2:
		loadModifiedModelFromFile = true;
		break;
    default:
        break;
    }
    glutPostRedisplay();
}



void keyboardSpecialUp(int key, int x, int y)
{
}

void mouseClick(int button, int state, int x, int y)
{
    if(state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
    {
        mouseClickDown = true;
        mx0 = x;
        my0 = y;
		checkForCollision = true;
		brushOn = true;
		selectAnchorPointOn = true;
		mouseClickForRayCastCheck = true;
		deformationOn = true; 
		handleDeformationOfNonOverlappedBrushedRegions = true;
		mouseClickCoordsVector.clear();
		mouseClickCoordsVector.push_back(mx0);
		mouseClickCoordsVector.push_back(my0);
		//cout << endl << "mx0: " << mx0;
		//cout << endl << "my0: " << my0;
		
    }
    else 
		mouseClickDown = false;
	glutPostRedisplay();
}

//modify rayCastPicking just to check if there is intersection between the ray-3d and the mesh
bool choseTheMeshWhileClicking()
{
	//find world coordinates of mouse's 'pixel' coords
	double vecBegin[3],vecEnd[3];
	get3DRay(vecBegin, vecEnd, mx0, my0);
	
	//define sphere
	double mass_center[3];
	
	
	MassCenterOfMesh(mesh,mass_center);
	float radius_of_sphere =  RadiusOfSphere(mesh,mass_center);
	
	//check for possibility of collision
	if(Ray3DIntersectsWithSphere(mass_center,radius_of_sphere,vecBegin,vecEnd))
	{
		vector<unsigned> &indices = mesh.indices;
		vector<float> &positions = mesh.positions;
		vector<float> intersection_pt_of_tris_and_ray3d(0);
		

		for(int i = 0 ; i < mesh.indices.size() ; i+=3)
		{
			float vert1[3] = {positions[3*indices[i]], positions[3*indices[i] + 1], positions[3*indices[i] + 2]};
			float vert2[3] = {positions[3*indices[i + 1]], positions[3*indices[i + 1] + 1], positions[3*indices[i + 1] + 2]};
			float vert3[3] = {positions[3*indices[i + 2]], positions[3*indices[i + 2] + 1], positions[3*indices[i + 2] + 2]};
			
			if(TriIntersectsWithRay3D(vert1,vert2,vert3,vecBegin,vecEnd,vecBegin,intersection_pt_of_tris_and_ray3d))
				return true;
		}

		return false;
	}
	else
		return false;
}


void mouseMotion(int x, int y)
{
    if (mouseClickDown)
    {
		if((!choseTheMeshWhileClicking() && !(mode & EX3B)) || ((EX3B & mode) && cameraOn) )
		{
			// Calculate angles
			azimuthAngle  -= (x-mx0)*rotFactor;
			altitudeAngle -= (y-my0)*rotFactor;
			// Set new camera position
			cx = cdist*sin(altitudeAngle)*sin(azimuthAngle);
			cy = cdist*cos(altitudeAngle);
			cz = cdist*sin(altitudeAngle)*cos(azimuthAngle);
		}
		// Keep mouse x,y for next call
        mx0 = x;
        my0 = y;
		brushOn = true;
		deformationOn = true;
        glutPostRedisplay();
    }
	
}
